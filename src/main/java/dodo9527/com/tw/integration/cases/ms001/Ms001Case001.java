package dodo9527.com.tw.integration.cases.ms001;

import dodo9527.com.tw.integration.cases.GeneralTestCase;
import dodo9527.com.tw.integration.cases.model.TestCaseDescModel;
import dodo9527.com.tw.integration.cases.model.TestContextExchange;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;


@Slf4j
@Component(value = "Ms001Case001")
public class Ms001Case001 extends GeneralTestCase {


    public Ms001Case001() {
        super();
        TestCaseDescModel testCaseDescModel = TestCaseDescModel.builder()
                .sheetId("MS001")
                .caseNo("001")
                .testSubject("登錄 ms hub 管理介面")
                .optStep(
                        "\"1. 登錄 ms hub 管理介面\n" +
                                "2. 輸入帳號\n" +
                                "3. 輸入密碼\n" +
                                "4. 點選登錄\"")
                .expectedResult("進入 ms hub 管理介面")
                .testUrl("http://43.249.100.202/hub/admin/login.htm").build();
        this.setTestCaseDescModel(testCaseDescModel);
    }


    @Override
    public void run(TestContextExchange pExchagne) {
        WebDriver webDriver = pExchagne.getWebDriver();
        webDriver.findElement(By.name("__name")).sendKeys("QA_TEST");
        webDriver.findElement(By.name("password")).sendKeys("aa666666");
        webDriver.findElement(By.name("submit")).click();


    }

    @Override
    public boolean assertion(TestContextExchange pExchange) {
        WebDriver webDriver = pExchange.getWebDriver();
        String currentUrl = webDriver.getCurrentUrl();
        return currentUrl.endsWith("hub/index.htm");

    }
}
