package dodo9527.com.tw.integration.config;


import dodo9527.com.tw.integration.config.type.BrowserType;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeDriverService;
import org.openqa.selenium.edge.EdgeDriverService;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.GeckoDriverService;
import org.openqa.selenium.ie.InternetExplorerDriverService;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.net.URL;


@Configuration
@ComponentScan({"dodo9527.com.tw.integration.cases", "dodo9527.com.tw.integration.service", "dodo9527.com.tw.integration.controller"})
@EnableMongoRepositories(basePackages = {"dodo9527.com.tw.integration.dao"})
@EnableAsync
@EnableSwagger2
@Slf4j
public class IntegrationConfig {


    @Value("#{'${derder.browser.driver.type}'.toUpperCase()}")
    BrowserType browserType;

    @Value("${derder.browser.driver.path}")
    String driverPath;

    @Value("${derder.browser.remote.url}")
    String seleniumHubUrl;


    @Bean(name = "webDriver")
    @Scope("prototype")
    public WebDriver initWebDriver(DesiredCapabilities desiredCapabilities) throws Exception {
        WebDriver webDriver = null;
        switch (this.browserType) {
            case CHROME:
                System.setProperty(ChromeDriverService.CHROME_DRIVER_EXE_PROPERTY, this.driverPath);
                webDriver = new ChromeDriver();
                break;
            case FIREFOX:
                System.setProperty(GeckoDriverService.GECKO_DRIVER_EXE_PROPERTY, this.driverPath);
                webDriver = new FirefoxDriver();
                break;
            case IE:
                System.setProperty(InternetExplorerDriverService.IE_DRIVER_EXE_PROPERTY, this.driverPath);
                webDriver = new FirefoxDriver();
                break;
            case EDGE:
                System.setProperty(EdgeDriverService.EDGE_DRIVER_EXE_PROPERTY, this.driverPath);
                webDriver = new FirefoxDriver();
                break;
            case REMOTE_CHROME:
                webDriver = new RemoteWebDriver(new URL(this.seleniumHubUrl), desiredCapabilities);
                break;
            case REMOTE_FIREFOX:
                webDriver = new RemoteWebDriver(new URL(this.seleniumHubUrl), DesiredCapabilities.firefox());
                break;


        }

        log.info("=========init webDriver:{} : {}{} ================", this.browserType, this.driverPath, this.seleniumHubUrl);
        webDriver.manage().window().maximize();
        return webDriver;
    }


    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("dodo9527.com.tw.integration.controller")).paths(PathSelectors.any()).build();
    }
}
