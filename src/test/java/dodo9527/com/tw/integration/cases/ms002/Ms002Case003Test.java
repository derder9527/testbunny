package dodo9527.com.tw.integration.cases.ms002;

import dodo9527.com.tw.integration.cases.GeneralTest;
import dodo9527.com.tw.integration.cases.GeneralTestCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Ms002Case003Test extends GeneralTest {


    @Autowired
    @Qualifier(value = "Ms002Case003")
    Ms002Case003 ms002CASE003;


    @Override
    protected GeneralTestCase getTestCase() {
        return this.ms002CASE003;
    }

}