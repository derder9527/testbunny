package dodo9527.com.tw.integration.cases.ms001;

import dodo9527.com.tw.integration.cases.GeneralTestCase;
import dodo9527.com.tw.integration.cases.model.TestCaseDescModel;
import dodo9527.com.tw.integration.cases.model.TestContextExchange;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.springframework.stereotype.Component;


@Slf4j
@Component(value = "Ms001Case002")
public class Ms001Case002 extends GeneralTestCase {


    public Ms001Case002() {
        super();
        TestCaseDescModel testCaseDescModel = TestCaseDescModel.builder()
                .sheetId("MS001")
                .caseNo("002")
                .testSubject("\"新建一般公司 (至少3間)\n" +
                        "※ A公司 添加 B公司 為 補貨公司\n" +
                        "※ 總控公司 添加 C公司 為 總控分公司\"")
                .optStep("\"1. 點選網站管理\n" +
                        "2. 點選新增網站\n" +
                        "3. 進入新建一般公司設置資料畫面\n" +
                        "4. 設置新一般公司資料完成\n" +
                        "5. 點選確定\n" +
                        "6. 跳出新增網站清單成功訊息\n" +
                        "7. 點選確定\n" +
                        "8. 進入網站管理畫面\n" +
                        "9. 點選重新生成配置\n" +
                        "10. 跳出重新生成公司配置成功訊息\n" +
                        "11. 點選確定\n" +
                        "12. 點選ABC盤 (啟用)\"")
                .expectedResult("新建一般公司成功")
                .testUrl("http://43.249.100.202/hub/admin/login.htm").build();
        this.setTestCaseDescModel(testCaseDescModel);
    }


    @Override
    public void run(TestContextExchange pExchagne) {
        log.info("login before operate ");
        WebDriver webDriver = pExchagne.getWebDriver();
        loginToMsHub(webDriver, "QA_TEST", "aa666666");
        //TODO 有點複雜先跳過

    }


    @Override
    public boolean assertion(TestContextExchange pExchange) {
        WebDriver webDriver = pExchange.getWebDriver();
        String currentUrl = webDriver.getCurrentUrl();
        return currentUrl.endsWith("hub/index.htm");

    }
}
