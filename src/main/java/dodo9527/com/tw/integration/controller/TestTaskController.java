package dodo9527.com.tw.integration.controller;


import com.opencsv.CSVWriter;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import dodo9527.com.tw.integration.model.OutputTestResultModel;
import dodo9527.com.tw.integration.service.TestManagementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("task")
@Slf4j
public class TestTaskController {


    @Autowired
    TestManagementService testManagementService;


    @RequestMapping(value = "/start", method = RequestMethod.GET)
    public String triggerTest() {
        String testId = UUID.randomUUID().toString();
        this.testManagementService.triggerTest(testId);
        return testId;
    }

    @RequestMapping(value = "/result/{testUid}", method = RequestMethod.GET)
    public List<OutputTestResultModel> queryTestResult(@PathVariable String testUid) {
        return this.testManagementService.queryResult(testUid);
    }

    @GetMapping("/export-result/{testUid}")
    public void exportCSV(HttpServletResponse response, @PathVariable String testUid) throws Exception {

        //set file name and content type
        String filename = "output.csv";
        PrintWriter responseWriter = response.getWriter();
        responseWriter.write(new String(new byte[]{(byte) 0xEF, (byte) 0xBB, (byte) 0xBF})); // 手動加BOM


        response.setContentType("text/csv");
        response.setHeader(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=\"" + filename + "\"");

        List<OutputTestResultModel> outputTestResultModels = this.testManagementService.queryResult(testUid);
        //create a csv writer
        StatefulBeanToCsv<OutputTestResultModel> writer = new StatefulBeanToCsvBuilder<OutputTestResultModel>(responseWriter)
                .withQuotechar(CSVWriter.NO_QUOTE_CHARACTER)
                .withSeparator(CSVWriter.DEFAULT_SEPARATOR)
                .withOrderedResults(false)
                .build();

        //write all users to csv file
        writer.write(outputTestResultModels);

    }
}
