package dodo9527.com.tw.integration.model;


import dodo9527.com.tw.integration.cases.model.TestCaseDescModel;
import dodo9527.com.tw.integration.model.type.TestResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TestResultDTO {


    private String testUid;

    private TestCaseDescModel testCaseDescModel;

    private String screenshotLocation;

    //測試開始時間
    private Date testStartTime;

    //測試結束時間
    private Date testEndTime;


    //OK , NG , ERROR
    private TestResult testResult;

    private String testDesc;


}
