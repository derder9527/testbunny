package dodo9527.com.tw.integration.cases;


import dodo9527.com.tw.integration.cases.model.TestContextExchange;
import dodo9527.com.tw.integration.config.IntegrationApplication;
import dodo9527.com.tw.integration.model.type.TestResult;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {IntegrationApplication.class})
public abstract class GeneralTest {

    private String uid;


    @Autowired
    WebDriver webDriver;

    @Before
    public void setUp() {
        this.uid = UUID.randomUUID().toString();
    }

    @After
    public void tearDown() {
    }

    @Test
    public void mainTest() {
        TestContextExchange exchange = TestContextExchange.builder().testUid(this.uid).webDriver(this.webDriver).build();
        GeneralTestCase testCase = this.getTestCase();
        try {
            testCase.setUp(exchange);
            testCase.run(exchange);
            if (testCase.assertion(exchange)) {
                testCase.success(exchange);
            } else {
                testCase.failure(exchange);
            }
        } catch (Exception e) {
            exchange.setOccurredException(e);
        } finally {
            testCase.endCase(exchange);
        }
        TestResult testResult = exchange.getTestResultDTO().getTestResult();

        Assert.assertEquals(TestResult.OK, testResult);
    }

    abstract protected GeneralTestCase getTestCase();


}
