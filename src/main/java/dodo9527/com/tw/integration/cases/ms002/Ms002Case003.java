package dodo9527.com.tw.integration.cases.ms002;

import dodo9527.com.tw.integration.cases.GeneralTestCase;
import dodo9527.com.tw.integration.cases.model.TestCaseDescModel;
import dodo9527.com.tw.integration.cases.model.TestContextExchange;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.stereotype.Component;


@Slf4j
@Component(value = "Ms002Case003")
public class Ms002Case003 extends GeneralTestCase {


    public Ms002Case003() {
        super();
        TestCaseDescModel testCaseDescModel = TestCaseDescModel.builder()
                .sheetId("MS002")
                .caseNo("003")
                .testSubject("再次添加補貨公司")
                .optStep("\"1. ms hub 管理介面\n" +
                        "2. 點選網站管理\n" +
                        "3. 點選定義補貨公司\n" +
                        "4. 勾選補貨公司\n" +
                        "5. 點選保存\n" +
                        "6. 跳出保存成功訊息\n" +
                        "7. 點選確定\"")
                .expectedResult("添加補貨公司成功")
                .testUrl("http://43.249.100.202/hub/admin/login.htm")
                .upSteamTestCase(Ms002Case002.class).build();
        this.setTestCaseDescModel(testCaseDescModel);
    }


    @Override
    public void run(TestContextExchange pExchagne) {

        WebDriver webDriver = pExchagne.getWebDriver();
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        loginToMsHub(webDriver, "QA_TEST", "aa666666");
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("網站管理")));
        webDriver.findElement(By.linkText("網站管理")).click();

        webDriver.switchTo().frame(0);
        wait.until(ExpectedConditions.elementToBeClickable(By.cssSelector("a[href*='../hubmanage/corp.htm?hub_id=7&corp_id=627&refer=list']")));
        webDriver.findElement(By.cssSelector("a[href*='../hubmanage/corp.htm?hub_id=7&corp_id=627&refer=list']")).click();
        wait.until(ExpectedConditions.elementToBeClickable(By.id("7_629")));
        webDriver.findElement(By.id("7_629")).click();
        webDriver.findElement(By.id("submit_button")).click();


    }


    @Override
    public boolean assertion(TestContextExchange pExchange) {
        WebDriver webDriver = pExchange.getWebDriver();
        String text = webDriver.switchTo().alert().getText();
        return "保存成功！".equals(text);

    }
}
