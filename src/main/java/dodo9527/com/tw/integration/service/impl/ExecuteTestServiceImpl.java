package dodo9527.com.tw.integration.service.impl;


import dodo9527.com.tw.integration.cases.GeneralTestCase;
import dodo9527.com.tw.integration.cases.model.TestCaseDescModel;
import dodo9527.com.tw.integration.cases.model.TestContextExchange;
import dodo9527.com.tw.integration.config.type.BrowserType;
import dodo9527.com.tw.integration.dao.TestResultEntityDao;
import dodo9527.com.tw.integration.entity.TestResultEntity;
import dodo9527.com.tw.integration.model.TestResultDTO;
import dodo9527.com.tw.integration.service.ExecuteTestService;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class ExecuteTestServiceImpl implements ExecuteTestService {

    @Value("#{'${derder.browser.driver.type}'.toUpperCase()}")
    BrowserType browserType;

    @Value("${derder.browser.driver.path}")
    String driverPath;

    @Value("${derder.browser.remote.url}")
    String seleniumHubUrl;


    @Autowired
    private ApplicationContext context;

    @Autowired
    private TestResultEntityDao testResultEntityDao;

    @Override
    @Async
    public void execute(List<GeneralTestCase> testCases, String testUid) {
        List<TestResultDTO> result = new ArrayList<>();

        for (GeneralTestCase testCase : testCases) {
            DesiredCapabilities chrome = DesiredCapabilities.chrome();
            TestCaseDescModel descModel = testCase.getTestCaseDescModel();
            String name = descModel.getSheetId() + "-" + descModel.getCaseNo();
            chrome.setCapability("name", name);
            chrome.setCapability("build", testUid);
            chrome.setCapability("tz", "Asia/Taipei");

            WebDriver webDriver = (WebDriver) this.context.getBean("webDriver", chrome);

            TestContextExchange exchange = TestContextExchange.builder().testUid(testUid).webDriver(webDriver).build();
            try {
                testCase.setUp(exchange);
                testCase.run(exchange);
                if (testCase.assertion(exchange)) {
                    testCase.success(exchange);
                    Cookie cookie = new Cookie("zaleniumTestPassed", "true");
                    exchange.getWebDriver().manage().addCookie(cookie);
                } else {
                    testCase.failure(exchange);
                    Cookie cookie = new Cookie("zaleniumTestPassed", "false");
                    exchange.getWebDriver().manage().addCookie(cookie);
                }
            } catch (Exception e) {
                exchange.setOccurredException(e);
            } finally {
                testCase.endCase(exchange);
            }
            result.add(exchange.getTestResultDTO());
        }


        result.forEach(r -> log.info(r.toString()));

        TestResultEntity entity = TestResultEntity.builder().testUid(testUid).resultDTOList(result).build();
        this.testResultEntityDao.save(entity);
    }


}
