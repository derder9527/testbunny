package dodo9527.com.tw.integration.cases.ms002;

import dodo9527.com.tw.integration.cases.GeneralTest;
import dodo9527.com.tw.integration.cases.GeneralTestCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Ms002Case001Test extends GeneralTest {


    @Autowired
    @Qualifier(value = "Ms002Case001")
    Ms002Case001 ms002CASE001;


    @Override
    protected GeneralTestCase getTestCase() {
        return this.ms002CASE001;
    }

}