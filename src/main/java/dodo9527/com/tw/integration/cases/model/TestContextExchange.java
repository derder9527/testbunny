package dodo9527.com.tw.integration.cases.model;

import dodo9527.com.tw.integration.model.TestResultDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.openqa.selenium.WebDriver;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TestContextExchange {

    WebDriver webDriver;

    //測試案例的描述檔
    TestCaseDescModel testCaseDescModel;

    // 每一次的測試都會gen一個uid來識別
    private String testUid;


    //測試期間有發生的錯誤
    private Exception occurredException;

    // 測試完畢後的結果
    private TestResultDTO testResultDTO;


}
