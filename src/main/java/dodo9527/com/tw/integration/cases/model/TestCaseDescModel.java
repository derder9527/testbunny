package dodo9527.com.tw.integration.cases.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Transient;

/**
 * 專門描述這個testCase的內容
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TestCaseDescModel {

    // sheet的id : ms-001
    private String sheetId;

    // sheet的name : 新建一般公司
    private String sheetName;

    // 測試案例的編號
    private String caseNo;

    // 測試項目
    private String testSubject;


    // 操作步驟
    private String optStep;

    // 預期結果
    private String expectedResult;

    // 備註
    private String ps;


    // 測試的起始網頁
    private String testUrl;

    //上游的測試案例
    @Transient
    private Class upSteamTestCase;
}
