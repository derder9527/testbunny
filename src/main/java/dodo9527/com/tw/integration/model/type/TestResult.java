package dodo9527.com.tw.integration.model.type;


/**
 * define test result when output
 */
public enum TestResult {
    OK,
    NG,
    ERROR
}
