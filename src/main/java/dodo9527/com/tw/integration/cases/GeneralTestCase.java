package dodo9527.com.tw.integration.cases;


import dodo9527.com.tw.integration.cases.model.TestCaseDescModel;
import dodo9527.com.tw.integration.cases.model.TestContextExchange;
import dodo9527.com.tw.integration.model.TestResultDTO;
import dodo9527.com.tw.integration.model.type.TestResult;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.springframework.beans.factory.annotation.Value;

import javax.imageio.ImageIO;
import java.awt.Rectangle;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Data
@Slf4j
@AllArgsConstructor
@NoArgsConstructor
public abstract class GeneralTestCase implements BaseTestCase {

    private TestCaseDescModel testCaseDescModel;


    @Value("${derder.screenshot.location}")
    private String screenshotBaseLocation;

    @Override
    public void setUp(TestContextExchange pExchange) {
        String testUid = pExchange.getTestUid();
        log.info("testUid:{} , startTest :{}-{}", testUid, this.testCaseDescModel.getSheetId(), this.testCaseDescModel.getCaseNo());
        WebDriver webDriver = pExchange.getWebDriver();
        TestResultDTO resultDTO = new TestResultDTO();
        resultDTO.setTestUid(testUid);
        resultDTO.setTestCaseDescModel(this.testCaseDescModel);
        resultDTO.setTestStartTime(new Date());
        pExchange.setTestResultDTO(resultDTO);
        pExchange.setTestCaseDescModel(this.testCaseDescModel);
        webDriver.get(this.testCaseDescModel.getTestUrl());


    }

    @Override
    public void success(TestContextExchange pExchange) {
        String testUid = pExchange.getTestUid();
        TestCaseDescModel pExchangeTestCaseDescModel = pExchange.getTestCaseDescModel();
        WebDriver webDriver = pExchange.getWebDriver();
        TestResultDTO resultDTO = pExchange.getTestResultDTO();
        String folderLocation = folderPathAppender(this.screenshotBaseLocation, testUid,
                pExchangeTestCaseDescModel.getSheetId(), pExchangeTestCaseDescModel.getCaseNo(), "success");
        String picLocation = savePic(webDriver, folderLocation);
        resultDTO.setScreenshotLocation(picLocation);
        resultDTO.setTestResult(TestResult.OK);
        resultDTO.setTestDesc("ALL IS WELL");

    }

    @Override
    public void failure(TestContextExchange pExchange) {
        String testUid = pExchange.getTestUid();
        TestCaseDescModel pExchangeTestCaseDescModel = pExchange.getTestCaseDescModel();
        WebDriver webDriver = pExchange.getWebDriver();
        TestResultDTO resultDTO = pExchange.getTestResultDTO();
        String folderLocation = folderPathAppender(this.screenshotBaseLocation, testUid,
                pExchangeTestCaseDescModel.getSheetId(), pExchangeTestCaseDescModel.getCaseNo(), "failure");
        String picLocation = savePic(webDriver, folderLocation);
        resultDTO.setScreenshotLocation(picLocation);
        resultDTO.setTestResult(TestResult.NG);
        resultDTO.setTestDesc("B.A.D");
    }


    @Override
    public void endCase(TestContextExchange pExchange) {
        Exception testException = pExchange.getOccurredException();
        TestCaseDescModel pExchangeTestCaseDescModel = pExchange.getTestCaseDescModel();
        String testUid = pExchange.getTestUid();
        WebDriver webDriver = pExchange.getWebDriver();
        TestResultDTO resultDTO = pExchange.getTestResultDTO();
        if (testException != null) {
            resultDTO.setTestResult(TestResult.ERROR);
            String errorDesc = testException.getMessage() + ":" + testException.getCause();
            resultDTO.setTestDesc(errorDesc);
            log.error("error occur testUid:{}  case:{}-{} , due to :{}", pExchange.getTestUid(),
                    pExchangeTestCaseDescModel.getSheetId(), pExchangeTestCaseDescModel.getCaseNo(),
                    errorDesc);
        }

        resultDTO.setTestEndTime(new Date());
        try {
            if (testException != null) {
                String folderLocation = folderPathAppender(this.screenshotBaseLocation, testUid,
                        pExchangeTestCaseDescModel.getSheetId(), pExchangeTestCaseDescModel.getCaseNo(), "error");
                String picLocation = savePic(webDriver, folderLocation);
                resultDTO.setScreenshotLocation(picLocation);
            }
            pExchange.getWebDriver().quit();
        } catch (Exception e) {
            log.error("error when  endCase  , {}-{}", e.getMessage(), e.getCause());
        }


    }


    protected void loginToMsHub(WebDriver webDriver, String account, String password) {
        String loginUrl = "http://43.249.100.202/hub/admin/login.htm";
        webDriver.get(loginUrl);
        webDriver.findElement(By.name("__name")).sendKeys("QA_TEST");
        webDriver.findElement(By.name("password")).sendKeys("aa666666");
        webDriver.findElement(By.name("submit")).click();
    }

    //可能會拍好幾個快照，留下編號的空間
    protected String savePic(WebDriver pWebDriver, String folderLocation) {

        String fileLocation;
        int picIndex;
        File saveFolder = new File(folderLocation);
        File[] files = saveFolder.listFiles();
        if (saveFolder.exists() && files != null) {
            picIndex = files.length + 1;
        } else {
            picIndex = 1;
        }
        fileLocation = folderLocation + "/" + picIndex + ".jpg";
        log.debug("save screenshot  :{}", fileLocation);

        try {
            Thread.sleep(200);//睡一下讓螢幕ready 不然會拍到一些初始動畫
            Alert alert = pWebDriver.switchTo().alert();
            if (alert != null) {
                System.setProperty("java.awt.headless", "false");
                BufferedImage image = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
                File imgFile = new File(fileLocation);
                //noinspection ResultOfMethodCallIgnored
                imgFile.getParentFile().mkdirs();
                ImageIO.write(image, "jpg", new File(fileLocation));

            }
        } catch (NoAlertPresentException noAlert) { // if there is no alert in browser , this exception will throw out
            try {
                File screenshot = ((TakesScreenshot) pWebDriver).getScreenshotAs(OutputType.FILE);
                FileUtils.copyFile(screenshot, new File(fileLocation));
            } catch (Exception e) {
                log.error("error occur when try use Robot take screenshot {}:{}", e.getMessage(), e.getCause());
            }

        } catch (Exception e) {
            log.error("error occur when try to take screenshot {}:{}", e.getMessage(), e.getCause());
        }

        return fileLocation;
    }

    protected String folderPathAppender(String baseLocation, String... words) {
        List<String> appenderWords = Arrays.asList(words);
        StringBuilder sb = new StringBuilder();
        sb.append(baseLocation);
        appenderWords.forEach(word -> sb.append("/").append(word));
        return sb.toString();
    }


}
