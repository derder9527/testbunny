
### 啟動

> remember to install maven before anything 


- edit `application.yml` to fit your own environment (請看註解)
```yaml

derder:
  screenshot:
    location: /Users/derder/myData/tmp/integration/screenshot # 放螢幕截圖的地方
  browser:
    driver:
      type: "REMOTE_CHROME"  # 請看IntegrationConfig 指定selenium driver 路徑
      path: "/Users/derder/myData/projects/xing-an/integration/src/main/resources/webDriver/mac/chromedriver"
    remote:
      url: "http://localhost:4444/wd/hub" # remote的 selenium grid的路徑

spring:
  data:
    mongodb:
      uri: mongodb://root:password@localhost:27017/admin # data storage at mongo , key is testUid
      security:
        enabled: true
server:
  servlet:
    context-path: /autoTest
  port: 9487
```

- execute script to start service 
```bash
sh run.sh
```


### 程式架構 

- 非常基本的java spring boot application 

- 每個測試都是一個新的selenium webdriver , 不會相互影響 , 但有預留測試順序的空間

- mvc 

- 每個測試案例請按照excel上的sheet+caseNo編號


### clzs 

- IntegrationConfig 基本的設定都在這

- BaseTestCase  每個testCase都要實作這個，也可以繼承 `GeneralTestCase` 再override進行客製 

- TestContextExchange testCase每個流程之間傳遞資訊的物件

- TestManagementService 實作類別需要重寫，因為時間的關係這個實作類別算是最小可行版本，這個類別主要負責處理要把哪些測試案例送去執行
，而目前是用hardcode做完一個流程而已

- ExecuteTestService 實際執行測試案例的類別(has to be Async)

- GeneralTest 撰寫新的測試案例時可以開一個單元測試類別直接繼承這個類別，再把selenium webdriver 切到local端，
就可以在本機邊執行測試腳本邊寫囉(懶得切來切去可以用spring-profile-active的方式切yaml檔)


### TODO

- script  & compose file 的參數化 

- log 

- zalenium dashboard 上失敗的測試應該可以特別標記出來

- zalenium test 時間的長度應該還可以再設定短一點 