#!/usr/bin/env bash

# create services
cd src/main/resources/zalenium
docker-compose up --force-recreate

# start test bunny
cd ../../../../
nohup mvn spring-boot:run &
