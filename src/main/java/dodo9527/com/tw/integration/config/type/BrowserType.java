package dodo9527.com.tw.integration.config.type;


/**
 * define which browser to use for testing webDriver
 */
public enum BrowserType {

    CHROME, FIREFOX, EDGE, IE,
    REMOTE_CHROME, REMOTE_FIREFOX
}
