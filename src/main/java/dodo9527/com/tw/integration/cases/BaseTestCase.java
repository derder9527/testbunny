package dodo9527.com.tw.integration.cases;

import dodo9527.com.tw.integration.cases.model.TestContextExchange;

public interface BaseTestCase {

    void setUp(TestContextExchange pExchange);

    void run(TestContextExchange pExchagne);

    boolean assertion(TestContextExchange pExchange);

    void success(TestContextExchange pExchange);

    void failure(TestContextExchange pExchange);

    void endCase(TestContextExchange pExchange);
}
