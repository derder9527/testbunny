package dodo9527.com.tw.integration.service;

import dodo9527.com.tw.integration.cases.GeneralTestCase;

import java.util.List;

public interface ExecuteTestService {

    public void execute(List<GeneralTestCase> testCases, String testUid);

}
