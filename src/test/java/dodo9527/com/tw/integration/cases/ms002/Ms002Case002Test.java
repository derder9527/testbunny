package dodo9527.com.tw.integration.cases.ms002;

import dodo9527.com.tw.integration.cases.GeneralTest;
import dodo9527.com.tw.integration.cases.GeneralTestCase;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class Ms002Case002Test extends GeneralTest {


    @Autowired
    @Qualifier(value = "Ms002Case002")
    Ms002Case002 ms002CASE002;


    @Override
    protected GeneralTestCase getTestCase() {
        return this.ms002CASE002;
    }

}