package dodo9527.com.tw.integration.service.impl;

import dodo9527.com.tw.integration.cases.GeneralTestCase;
import dodo9527.com.tw.integration.cases.model.TestCaseDescModel;
import dodo9527.com.tw.integration.dao.TestResultEntityDao;
import dodo9527.com.tw.integration.entity.TestResultEntity;
import dodo9527.com.tw.integration.model.OutputTestResultModel;
import dodo9527.com.tw.integration.model.TestResultDTO;
import dodo9527.com.tw.integration.service.ExecuteTestService;
import dodo9527.com.tw.integration.service.TestManagementService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


@Service
@Slf4j
public class TestManagementServiceImpl implements TestManagementService {

    @Autowired
    List<GeneralTestCase> testCases;

    @Autowired
    ExecuteTestService executeTestService;

    @Autowired
    private TestResultEntityDao testResultEntityDao;

    public static final Map<String, List<TestResultDTO>> testResultMap = new HashMap<>();


    @Override
    public List<OutputTestResultModel> queryResult(String testUid) {
        log.info("query testUid:{}", testUid);
        Optional<TestResultEntity> byId = this.testResultEntityDao.findById(testUid);
        if (byId.isPresent()) {
            List<TestResultDTO> resultDTOList = byId.get().getResultDTOList();
            List<OutputTestResultModel> output = resultDTOList.stream().map(r -> {
                TestCaseDescModel desc = r.getTestCaseDescModel();
                return OutputTestResultModel.builder()
                        .sheetId(desc.getSheetId())
                        .sheetName(desc.getSheetName())
                        .caseNo(desc.getCaseNo())
                        .optStep(desc.getOptStep())
                        .testSubject(desc.getTestSubject())
                        .testResult(r.getTestResult())
                        .testDesc(r.getTestDesc()).build();
            }).collect(Collectors.toList());
            return output;


        }
        return null;
    }

    @Override
    public void triggerTest(String testId) {
        log.info("user trigger test ! testID:{}", testId);
        List<GeneralTestCase> ms002TestPackage = this.testCases.stream().filter(t -> t.getTestCaseDescModel().getSheetId().equals("MS002")).sorted((o1, o2) -> {
            Integer o1No = Integer.parseInt(o1.getTestCaseDescModel().getCaseNo());
            Integer o2No = Integer.parseInt(o2.getTestCaseDescModel().getCaseNo());
            return o1No.compareTo(o2No);
        }).collect(Collectors.toList());

        this.executeTestService.execute(ms002TestPackage, testId);
    }
}
