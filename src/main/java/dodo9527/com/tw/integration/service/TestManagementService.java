package dodo9527.com.tw.integration.service;

import dodo9527.com.tw.integration.model.OutputTestResultModel;

import java.util.List;

public interface TestManagementService {

    public void triggerTest(String testId);

    public List<OutputTestResultModel> queryResult(String testUid);
}
