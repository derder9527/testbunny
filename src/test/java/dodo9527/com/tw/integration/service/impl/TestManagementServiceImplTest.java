package dodo9527.com.tw.integration.service.impl;

import dodo9527.com.tw.integration.config.IntegrationApplication;
import dodo9527.com.tw.integration.service.TestManagementService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.UUID;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {IntegrationApplication.class})
public class TestManagementServiceImplTest {


    @Autowired
    TestManagementService testManagementService;


    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void triggerTest() {
        String testId = UUID.randomUUID().toString();
        this.testManagementService.triggerTest(testId);
    }
}