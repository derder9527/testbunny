package dodo9527.com.tw.integration.entity;


import dodo9527.com.tw.integration.model.TestResultDTO;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "result")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TestResultEntity {

    @Id
    private String testUid;

    List<TestResultDTO> resultDTOList;

}
