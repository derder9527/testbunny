package dodo9527.com.tw.integration.dao;

import dodo9527.com.tw.integration.entity.TestResultEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestResultEntityDao extends MongoRepository<TestResultEntity, String> {
}
