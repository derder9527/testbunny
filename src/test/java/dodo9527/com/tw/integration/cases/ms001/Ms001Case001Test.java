package dodo9527.com.tw.integration.cases.ms001;

import dodo9527.com.tw.integration.cases.GeneralTest;
import dodo9527.com.tw.integration.cases.GeneralTestCase;
import dodo9527.com.tw.integration.config.IntegrationApplication;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {IntegrationApplication.class})
public class Ms001Case001Test extends GeneralTest {


    @Autowired
    @Qualifier(value = "Ms001Case001")
    Ms001Case001 ms001CASE001;


    @Override
    protected GeneralTestCase getTestCase() {
        return this.ms001CASE001;
    }
}