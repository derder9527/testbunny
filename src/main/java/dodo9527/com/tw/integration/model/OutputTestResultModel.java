package dodo9527.com.tw.integration.model;


import com.opencsv.bean.CsvBindByPosition;
import dodo9527.com.tw.integration.model.type.TestResult;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OutputTestResultModel {

    // sheet的id : ms-001
    @CsvBindByPosition(position = 0)
    private String sheetId;

    // sheet的name : 新建一般公司
    @CsvBindByPosition(position = 1)
    private String sheetName;

    // 測試案例的編號
    @CsvBindByPosition(position = 2)
    private String caseNo;

    // 測試項目
    @CsvBindByPosition(position = 3)
    private String testSubject;

    // 操作步驟
    @CsvBindByPosition(position = 4)
    private String optStep;

    // 預期結果
    @CsvBindByPosition(position = 5)
    private String expectedResult;

    @CsvBindByPosition(position = 6)
    private TestResult testResult;

    @CsvBindByPosition(position = 7)
    private String testDesc;


}
